import java.io.IOException;
import java.util.Scanner;

public class Basico {
	
	public void basico() throws IOException {
		
		System.out.println("Los conceptos básicos incluyen los tipos primitivos, operadores, caracteres de escape "
				+ "operadores relacionales y logicos y los atajos de operador/asignacion. ");
		System.out.println("Seleccione una opción: ");
		System.out.println(" 0. Tipos basicos.");
		System.out.println(" 1. Operadores aritmeticos");
		System.out.println(" 2. Caracteres de escape");
		System.out.println(" 3. Operadores relacionales");
		System.out.println(" 4. Operadores logicos");
		System.out.println(" 5. Atajos operador/asignación");
		System.out.println(" 6. String");
		System.out.println(" 9. Salir");
		
		Scanner s = new Scanner(System.in);
		int ch = s.nextInt();
		
		switch (ch) {
		
		case 0:
			
			this.tipos();
			
		break;
		
		case 1:
			
			this.operArit();
			
		break;
		
		case 2:
			
			this.caracEsc();
			
		break;
		
		case 3:
			
			this.relacional();
			
		break;
		
		case 4:
			
			this.logic();
			
		break;
		
		case 5:
			
			this.atajoper();
			
		break;
		
		case 6:
			
			this.string();
			
		break;
		
		case 9:
			
			System.out.println("Saliendo al menu principal");
			System.out.println();
			Menu m = new Menu();
			m.menu();
			
		break;
		
		default:
			System.out.println("Opción no encontrada.");
			this.basico();
		break;
		
		}
		s.close();
		
	}
	
	void tipos() throws IOException {
		//Esto es un string conocido como bloque de texto. Se usa declarando un string y poniendo comillas dobles 3 veces.
		//Permite escribir bloques de texto sin la necesidad de usar caracteres de escape dentro
		String bt1 = """
				Tipo Primitivo:
					-boolean: true y false
					-byte: número entero de 8bits
					-char: caracter suelto
					-double: número con decimales de 64bits
					-float: número con decimales de 32bits
					-int: número entero de 32bits
					-short: número entero de 16bits
					-long: número entero de 64bits
				-----------------------------------------------
				Tipo Literal:
					-Hexadecimal: declarado con 0x al principio:
						Como ejemplo: hex = 0xFF;
					-Octal: declarado empezando por 0
						Como empelo: oct = 011;				
				""";
		
		System.out.println(bt1);
		System.out.println("");
		
		this.basico();
		
		
	}
	
	void operArit() throws IOException {
		
		System.out.println("Los operadores aritmeticos en Java son los siguientes");
		System.out.println("+ suma");
		System.out.println("- resta");
		System.out.println("* multiplicación");
		System.out.println("/ division");
		System.out.println("% modulo. Este operador nos da el resto de una división");
		System.out.println("++ incremento");
		System.out.println("-- decremento");
		System.out.println("Con el incremento y el decremento podemos asignar un valor a una variable de la siguiente forma");
		int x = 10;
		int y = ++x;
		System.out.println("el valor de x es "+x+". Y el de y es "+y);
		int xx = 10;
		int yy= xx++;
		System.out.println("El valor de x es "+xx+". Y el de y es "+yy);
		System.out.println("En ambos casos el valor de ambos es el de x + 1, osea 11.");
		
		System.out.println("");
		this.basico();
		
	}
	
	void caracEsc() throws IOException {
		
		System.out.println("Los caracteres de escape");
		System.out.println("Los caracteres de escape se usan para poder añadir ciertos caracteres o saltos de linea por ejemplo aquí dentro de un sout \' ");
		System.out.println("El listado es: ");
		System.out.println("\' comilla simple	\" comilla doble	\\ barra inclinada	\r retorno de carro");
		System.out.println("\n linea nueva	\f salto de linea	\t tabulación	\b retroceso");
		System.out.println("");
		this.basico();
		
	}
	
	
	void relacional() throws IOException{
		
		System.out.println("Los operadores relacionales");
		System.out.println("Son los operadores utilizados para hacer comparativas, no solo entre números.");
		System.out.println("Son los siguientes: ");
		System.out.println("== igual a");
		System.out.println("!= distinto a");
		System.out.println("> mayor que");
		System.out.println("< menor que");
		System.out.println(">= mayor o igual a");
		System.out.println("<= menor o igual a");
		System.out.println("Ejemplo: ");
		
		int x = 5, y = 7;
		int[] tab1 = new int [10], tab2 = new int [8];
		
		if (x!=y) {
			System.out.println("Son distintos");
		}
		
		if(tab1 == tab2) {//Usamos el operador relacional == para comparar dos array.
			System.out.println("Son iguales");
		}else {
			System.out.println("Son distintos");
		}
		System.out.println("");
		this.basico();
		
	}
	
	void logic() throws IOException {
		
		System.out.println("Operadores logicos");
		System.out.println("Son los operadores para hacer operaciones logicas y solo funciona con tipo booleano, por lo tanto su resultado es un booleano.");
		System.out.println("Son los siguientes: ");
		System.out.println("& AND");
		System.out.println("| OR");
		System.out.println("^ XOR o OR exclusiva");
		System.out.println("|| OR cortocircuito");
		System.out.println("&& AND cortocircuito");
		System.out.println("! NOT");
		System.out.println("En java existen las versiones \"cortocircuito\" de AND y OR, la diferencia con la version normal es"
				+ " que las versiones normales evaluan cada operando, pero las cortocircuito no, solo lo hace cuando sea necesario.");
		System.out.println("");
		this.basico();
		
	}
	
	void atajoper() throws IOException {
		
		System.out.println("Los atajos de asignación");
		System.out.println("Se usan para hacer a la vez una operación y la asignación del resultado de la misma a una variable.");
		System.out.println("Son: ");
		System.out.println("+= suma		-= resta	*= multiplicación		/= división");
		System.out.println("%= modulo 	&= AND		|= OR		^= XOR");
		int x = 0 , y = 0;
		
		x = x + 10;
		y += 10;
		
		System.out.println("El valor de x es: "+x+" . El valor de y es: "+y);
		System.out.println("");
		this.basico();
		
	}

	void string() throws IOException {
		
		System.out.println("Los Strings son cadenas de texto, es mas, esto es un String que java convierte automaticamente");
		System.out.println("Pero para declarar un String tenemos que hacerlo: ");
		System.out.println("String str = new String (\"Esto es un String\");");
		System.out.println("o también: ");
		System.out.println("String str = \"Esto es un String\";");
		System.out.println("Hay varias operaciones que se pueden realizar con ellos como: ");
		System.out.println("string1.length() que devuelve el tamaño del string");
		System.out.println("string1.equals(string2) devuelve si contienen la misma secuencia de caracteres");
		System.out.println("string1.charAt(i) devuelve el caracter especificado en el indice. Puede ser i si lo recorremos con un for o número que haga de indice.");
		System.out.println("string1.compareTo(string2) nos da un int que si es menor que 0 es que string1 es menor que string2, si es mayor que 0 es que es mayor"
				+ " y si da 0 es que son iguales.");
		System.out.println("string1.indexOf(\"string\") busca en string1 el string que pasamos entre parentesis y devuelve la posición donde empieza la primera vez que"
				+ " ese string aparece en string1.");
		System.out.println("string1.lastIndexOf(\"string\") Igual que el anterior pero devuelve la posición del primer caracter de la última vez que aparece el string.");
		System.out.println("Ambos index devuelve -1 si no se encuentra el string");
		System.out.println("");
		System.out.println("Ejemplos: ");
		
		String string1 = new String ("Esto es un string de ejemplo");
		String string2 = "Este es el segundo string de ejemplo";
		String string3 = new String (string1);
		int resultado, index;
		
		System.out.println("El tamaño de string1 es: "+string1.length());
		
		for(int i=0; i< string1.length(); i++) {
			System.out.print(string1.charAt(i));
		}
		System.out.println("");
		
		if(string1.equals(string2))
			System.out.println("Son iguales string1 y string2");
		else
			System.out.println("No son iguales string1 y string2");
		if(string1.equals(string3))
			System.out.println("Son iguales string1 y string3");
		else
			System.out.println("No son iguales string1 y string3");
		
		resultado = string1.compareTo(string2);
		if(resultado == 0)
			System.out.println("String1 y string2 son iguales");
		else if(resultado <0)
			System.out.println("String1 es menor que string2");
		else if(resultado >0)
			System.out.println("String1 es mayor que string2");
		else
			System.out.println("Esto no debería salir.");
		
		string2="uno dos tres cuatro uno";
		
		index = string2.indexOf("uno");
		System.out.println("La primerar igualdad de uno es "+index);
		index = string2.lastIndexOf("uno");
		System.out.println("La última igualdad de uno es: "+index);
		
		System.out.println("Otra cosa que podemos hacer con los string es concatenarlos usando + ");
		String string4 = string1+string2;
		System.out.println(string4);
		
		System.out.println("");
		String bt0 = """ 
				O usar un substring que es una variable donde guardamos un fragmento de un string 
				y se haría indicando desde el primer al último caracter que quieres copiar en el substring.
				se declararía como: 
				String substring(int comienzo, int final);
				Ejemplo: 
				""";
		System.out.println(bt0);
		String substr = string4.substring(3, 20);
		System.out.println(string4);
		System.out.println(substr);
		
		System.out.println("");
		this.basico();
	}
	
}
