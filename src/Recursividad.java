import java.io.IOException;
import java.util.stream.IntStream;

public class Recursividad {

    public void pruebaRecursiva() throws IOException {

        System.out.println("Suma iterativa "+sumaIterativa(5));
        System.out.println("Suma recursiva "+sumaRecursiva(5));
        System.out.println("Suma factorial "+sumaFuncional(5));

        System.out.println("Factorial iterativo "+factorialIterativo(5));
        System.out.println("Factorial recursivo "+factorialRecursivo(5));
        System.out.println("Factorial funcional "+factorialFuncional(5));

        System.out.println(" ");
        Menu m = new Menu();
        m.menu();
    }

    // Un metodo iterativo (Se usa un for) para hacer una suma.
    public static int sumaIterativa(int numero){
        int resultado = 0;
        for (int i = 0; i <= numero; i++){
            resultado = resultado + i;
        }
        return resultado;
    }

    // Lo mismo pero usando recursividad. Esto es que se llame la funcion a si misma.
    public static int sumaRecursiva(int numero){
        // Esto es una sentencia de control, se tiene que poner siempre, si no la funcion entra en bucle hasta el
        // infinito. Se trata de añadirle una condición que al cumplirse salga de la funcion.
        if (numero == 1){
            return 1;
        }
        // Con este return llamamos a la funcion de forma recursiva para hacer la suma del numero. Restando 1 al numero
        // cada vez. Funciona distinto al for que va sumando 1, 2, 3, ... hasta cumplir la condicion de i<=numero
        // En este caso va desde el 5, 4, 3, ... Cuando numero llega al 1 vuelve hacia atras. Usar Debbuger para verlo.
        // Que para hacer esto hace uso de la pila y es mejor verlo en directo para comprender el funcionamiento.
        return numero + sumaRecursiva(numero -1);
    }

    // La recursion puede ser de dos tipos tail (de cola) o head(de cabeza) esto lo define si el momento donde se
    // llama a la funcion es al principio o al final, aqui hay dos ejemplos, tail donde primero se muetra el sout y
    // luego se llama a la funcion y de head, donde primero se llama a la funcion y luego se muestra el sout.
    public static void tailRecusion(int numero){
        if (numero == 0){
            return;
        }// Aqui mostramos el valor, se llama a la funcion, se muestra el valor, se llama a la funcion...
        // Así hasta que se cumple la condicion de break.
        System.out.println(numero);
        tailRecusion(numero -1);
    }
    public static void headRecursion(int numero){
        if (numero == 0){
            return;
        }// Pero aqui primero se llama a la funcion de forma seguida hasta que se cumple la condicion, quedando todos
        // los valores de numero almacenados en la pila para una vez cumplida la condicion se va mostrando
        // uno a uno cada valor.
        headRecursion(numero - 1);
        System.out.println(numero);
    }

    // Y el mismo ejemplo de la suma, pero de forma funcional con una lambda usando reduccion.
    public static int sumaFuncional(int numero){
        return IntStream.rangeClosed(1,5).reduce(0,(a, b) -> a + b);
    }

    // Otro ejemplo con las tres formas (iterativa, recursiva y funcional) para hacer el factorial de un numero.

    //El primer caso es el iterativo, usando un for.
    public static int factorialIterativo(int numero){
        int resultado = 1;
        for (int i = 1; i <= numero; i++){
            resultado = resultado * i;
        }
        return resultado;
    }
    // El factorial recursivo. Se pone la sentencia de control y el return con la funcion. En este caso es recursividad
    // de cola (tail) porque el return con la funcion recursiva es lo último que se ejecuta en el codigo (Primero se
    // comprueba la sentencia de control).
    public static int factorialRecursivo (int numero){
        if (numero == 1){
            return 1;
        }
        return numero * factorialRecursivo(numero -1);
        // Si hubiese mas codigo aqui sería de cabeza (head), al realizarse el return y despues el codigo.
    }
    // Y al forma funcional.
    public static int factorialFuncional(int numero){
        return IntStream.rangeClosed(1,numero).reduce(1, (a,b)-> a * b);
    }
}
