package Patrones.Estructurales.Decorator;

import Patrones.Patrones;

public class Decorator {
    public void decorator (){
        TelefonoBasico telefonobasico = new TelefonoBasico();
        telefonobasico.crear();

        TelefonoInteligente telefonointeligente = new TelefonoInteligente(new TelefonoBasico());
        telefonointeligente.crear();

        TelefonoNextGen telefononextgen = new TelefonoNextGen(new TelefonoInteligente(new TelefonoBasico()));
        telefononextgen.crear();

        TelefonoNextGen telefononextgen1 = new TelefonoNextGen(new TelefonoBasico());
        telefononextgen1.crear();

        Patrones patr = new Patrones();
        patr.menuPatrones();
    }

}
