package Patrones.Estructurales.Adapter;

public interface Enchufable {
    void enciende();
    void apaga();
    boolean estaEncendido();

}
