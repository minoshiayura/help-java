package Patrones.Estructurales.Adapter;

import Patrones.Patrones;

/**
 * El patron Adapter permite que dos objetos con interfaces incompatibles trabajen juntos. Este patrón se aplica cuando
 * queremos utilizar una clase existente y su interfaz no es compatible con el resto de las clases en nuestro sistema.
 * En lugar de modificar la clase existente o crear una nueva que sea compatible, creamos un adapter que actúa como un
 * puente entre ambas. En este ejemplo tenemos el horno y la lampara que funcionan con una interfaz Enchufable y
 * Lightbulb que sería la clase incompatible. Para ello creamos la clase PowerAdapter que hereda de Enchufable y adapta
 * los metodos a los de lightbulb.
 */
public class Adapter {

    public void adapter(){
        //Creamos los objetos.
        Horno horno = new Horno();
        Lampara lampara = new Lampara();
        PowerAdapter lightbulb = new PowerAdapter();
        // llamamos a los metodos de cada clase usando el mismo metodo aunque sean clases diferentes o incompatibles.
        enciende(horno);
        enciende(lampara);
        enciende(lightbulb);

        estaEncendido(horno);
        estaEncendido(lampara);
        estaEncendido(lightbulb);

        apaga(horno);
        apaga(lampara);
        apaga(lightbulb);

        estaEncendido(horno);
        estaEncendido(lampara);
        estaEncendido(lightbulb);

        Patrones patr = new Patrones();
        patr.menuPatrones();
    }

    public void enciende(Enchufable enchufable){
        enchufable.enciende();
        System.out.println("Encendiendo "+enchufable);
    }

    public void apaga(Enchufable enchufable){
        enchufable.apaga();
        System.out.println("Apagando: "+enchufable);
    }

    public boolean estaEncendido(Enchufable enchufable){
        System.out.println(enchufable.estaEncendido());
        return enchufable.estaEncendido();
    }
}
