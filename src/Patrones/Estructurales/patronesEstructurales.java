package Patrones.Estructurales;

import Patrones.Estructurales.Adapter.Adapter;
import Patrones.Estructurales.Decorator.Decorator;
import Patrones.Patrones;

import java.util.Scanner;

public class patronesEstructurales {

    public void menuEstructurales(){
        System.out.println("Este es el menu de los patrones estructurales.");
        System.out.println("Que patrón quieres ver: ");
        System.out.println("Decorator o Adapter o menu (para el menu anterior)");

        Scanner scanner = new Scanner(System.in);
        String choice = scanner.next();

        if(choice.equalsIgnoreCase("decorator")){
            System.out.println("Has elegido la opcion del patron Decorator.");
            Decorator deco = new Decorator();
            deco.decorator();
        }else if(choice.equalsIgnoreCase("adapter")){
            System.out.println("Has elegido la opcion del patron Adapter.");
            Adapter adapter = new Adapter();
            adapter.adapter();
        }else if(choice.equalsIgnoreCase("menu")){
            System.out.println("Volviendo al menu anterior");
            Patrones patr = new Patrones();
            patr.menuPatrones();
        } else{
            System.out.println("No has elegido una opcion correcta.");
            menuEstructurales();
        }
    }
}
