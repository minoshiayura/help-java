package Patrones;

import Patrones.Comportamiento.patronesComportamiento;
import Patrones.Estructurales.patronesEstructurales;
import Patrones.Creacionales.patronesCreacionales;

import java.util.Scanner;

public class Patrones {

    public void menuPatrones(){
        System.out.println("Que tipo de patrones quieres ver: ");
        System.out.println("estructurales - comportamiento - creacional");
        Scanner scanner = new Scanner(System.in);
        String choice = scanner.next();

        if (choice.equalsIgnoreCase("estructurales")){
            patronesEstructurales patest = new patronesEstructurales();
            patest.menuEstructurales();
        } else if (choice.equalsIgnoreCase("comportamiento")){
            patronesComportamiento patcom = new patronesComportamiento();
            patcom.menuComportamiento();
        } else if (choice.equalsIgnoreCase("creacional")){
            patronesCreacionales patcrea = new patronesCreacionales();
            try {
                patcrea.menuCreacionales();
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }

        } else {
            System.out.println("Volviendo al menu principal.");
        }
    }
}
