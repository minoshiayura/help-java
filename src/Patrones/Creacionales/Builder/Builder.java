package Patrones.Creacionales.Builder;

/**
 * El patron Builder se utiliza para crear objetos complejos con varios atributos o valores. En lugar de llamar a
 * varios constructores o metodos setters para establecer cada valor. El patrón Builder proporciona una interfaz que
 * permite establecer los valores de forma secuencial.
 */
public class Builder {
    public void builder(){
        // En lugar de usar el constructor clásico:
        // Vehiculo vehiculo = new Vehiculo();
        // Se usa este constructor:
        Vehiculo coche = new CocheBuilder("Opel")
                .setMotor("Diesel")
                .setPuertas(4)
                .setTipo("Combustion")
                .build();

        System.out.println(coche);
    }
}
