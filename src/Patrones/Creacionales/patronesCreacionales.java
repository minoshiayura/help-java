package Patrones.Creacionales;

import Patrones.Creacionales.Builder.Builder;
import Patrones.Creacionales.Factory.Factory;
import Patrones.Creacionales.Prototype.Prototype;
import Patrones.Creacionales.Singleton.MainSingleton;
import Patrones.Patrones;

import java.util.Scanner;

public class patronesCreacionales {

    public void menuCreacionales() throws CloneNotSupportedException {
        System.out.println("Este es el menu de los patrones creacionales.");
        System.out.println("Elige entre los siguientes patrones: ");
        System.out.println("Builder - Factory - Prototype - Singleton");

        Scanner scanner = new Scanner(System.in);
        String choice = scanner.next();
        choice = choice.toLowerCase();

        switch (choice){
            case "builder":
                System.out.println("Has elegido el patron Builder.");
                Builder buil = new Builder();
                buil.builder();
            break;
            case "factory":
                System.out.println("Has elegido el patron Factory.");
                Factory fact = new Factory();
                fact.factory();
            break;
            case "prototype":
                System.out.println("Has elegido el patron Prototype");
                Prototype proto = new Prototype();
                try{
                    proto.prototype();
                }catch(CloneNotSupportedException e){
                    throw new CloneNotSupportedException(e.getMessage());
                }
            break;
            case "singleton":
                System.out.println("Has elegido el patron Singleton.");
                MainSingleton sing = new MainSingleton();
                sing.mainSingleton();
            break;
            default:
                Patrones patr = new Patrones();
                patr.menuPatrones();
            break;

        }
    }
}
