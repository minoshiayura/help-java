package Patrones.Creacionales.Prototype;

/**
 * El patron Prototupe sirve para crear nuevos objetos a partir de objetos existentes sin necesidad de concoer el tipo
 * de objeto que se va a crear. En este ejemplo se hace de dos formas con un metodo clonar "casero" como el que usa la
 * clase Coche o implementado la interfaz de Java "Cloneable" como lo hace la clase Moto.
 */
public class Prototype {

    public void prototype() throws CloneNotSupportedException {
        //Creamos un objeto coche con los atributos que tiene.
        Coche coche = new Coche();
        coche.puertas = 5;
        coche.marca = "Opel";
        coche.modelo = "Corsa";
        System.out.println(coche.marca+" "+coche.modelo+" "+coche.puertas);
        // Clonamos el coche cambiando el valor de puertas.
        Coche clonado = coche.clonar();
        clonado.puertas = 3;
        System.out.println(clonado.marca+" "+clonado.modelo+" "+clonado.puertas);
        // Creamos la moto
        Moto moto = new Moto();
        moto.ruedas = 2;
        moto.marca = "Kawasaki";
        moto.modelo = "Ninja";
        System.out.println(moto.marca+" "+moto.modelo+" "+moto.ruedas);
        // Y la clonamos cambiando el valor de ruedas.
        Moto clonada = moto.clonar();
        moto.ruedas = 3;
        System.out.println(moto.marca+" "+moto.modelo+" "+moto.ruedas);
    }
}
