package Patrones.Creacionales.Prototype;

public class Moto implements Cloneable {
    public String marca;
    public String modelo;
    public int ruedas;

    public Moto(){}
    // En el caso de la moto se usa la interfaz Cloneable de java. Es mucho mas sencillo. Solo hay que controlar las
    // excepciones.
    public Moto clonar() throws CloneNotSupportedException {
        // El (Moto) es un cast que fuerza a devolver el tipo que está entre parentesis.
        return (Moto)this.clone();
    }

}