package Patrones.Creacionales.Prototype;

public class Coche{
    public String marca;
    public String modelo;
    public int puertas;

    public Coche(){}
    //En el metodo clonar creamos el nuevo objeto clon e igualamos los valores del objeto con el que usamos el metodo.
    // Y devolvemos el clon.
    public Coche clonar(){
        Coche clon = new Coche();

        clon.marca = marca;
        clon.modelo = modelo;
        clon.puertas = puertas;

        return clon;
    }

}
