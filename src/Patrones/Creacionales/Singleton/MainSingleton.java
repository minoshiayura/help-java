package Patrones.Creacionales.Singleton;

/**
 * El patron Singleton es un patron que se usa para garantizar que  una clase solo tengauna unica instancia y
 * proporciona un punto de acceso globar a ella.
 */
public class MainSingleton {
    public void mainSingleton() {
        //Creamos los objetos sigleton con distintos valores para demostrar que los dos objetos tienen la misma
        // instancia.
        Singleton singleton = Singleton.getInstance();
        singleton.setContador(15);
        System.out.println(singleton.getContador());
        System.out.println(singleton);

        Singleton singleton2 = Singleton.getInstance();
        singleton2.setContador(10);
        System.out.println(singleton2.getContador());
        System.out.println(singleton2);



    }

}