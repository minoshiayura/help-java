package Patrones.Creacionales.Singleton;

/**
 * Para usar singleton tenemos que crear un constructor privado, definir una variable estatica privada y definir un
 * metodo estatico publico para obtener la unica instancia de la clase.
 */
public class Singleton {
    // Esta es la variable estatica privada.
    private static Singleton singleton;
    int contador;
    // Aqui esta el constructor privado.
    private Singleton(){}
    // Este metodo estatico publico comprueba si la variable es igual a null, si lo es, crea la instancia. Y devuelve
    // la variable. Con esto nos aseguramos de que siempre tendremos la misma instancia.
    public static Singleton getInstance() {
        if(singleton == null){
            singleton = new Singleton();
        }
        return singleton;
    }
    // Los metodos para establecer y conseguir el valor de contador.
    public int getContador() {
        return contador;
    }
    public void setContador(int contador) {
        this.contador = contador;
    }

}
