package Patrones.Creacionales.Factory;

/**
 * En la interfaz solo tenemos el metodo getPrecio
 */
public interface Precio {

    double getPrecio();

}
