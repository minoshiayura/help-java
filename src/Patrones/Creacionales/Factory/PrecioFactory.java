package Patrones.Creacionales.Factory;

/**
 * En el factory tenemos la lógica que es un if para averiguar hacia que clase derivar la construccion.
 */
public class PrecioFactory {
    Precio precio;
    //Constructor privado para que se tenga que usar el publico.
    private PrecioFactory(){}
    // El constructor publico con el argumento del pais para que el if pueda derivar a un constructor u otro.
    public PrecioFactory (String pais){
        // En lugar de poner solo equals se pone equalsIgnoreCase para que no importe si se escribe en mayusculas
        // o minusculas.
        if (pais.equalsIgnoreCase("España")){
            System.out.println("España, precio en EUR.");
            precio = new PrecioEUR();
        }else if (pais.equalsIgnoreCase("UK")){
            System.out.println("UK, precio en GBP");
            precio = new PrecioGBP();
        }else {
            System.out.println("Otro pais, precio en USD.");
            precio = new PrecioUSD();
        }
    }

    public double getPrecio(){
        return precio.getPrecio();
    }

}
