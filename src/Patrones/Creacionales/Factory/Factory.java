package Patrones.Creacionales.Factory;

/**
 * Se utiliza para crear objetos en una clase "fabrica" sin exponer la logica de creacion al cliente, permitiendole
 * trabajar cpn objetos a traves de una interfaz comun. En este ejemplo tenemos una clase Precio y sus subclases que
 * son distintas divisas. Dependiendo de la divisa que se pase al crear el objeto PrecioFactory se creara una u otra
 * moneda.
 */
public class Factory {

    public void factory(){
        PrecioFactory precio = new PrecioFactory("España");
        System.out.println(precio.getPrecio());

        PrecioFactory precio2 = new PrecioFactory("Usa");
        System.out.println(precio2.getPrecio());

        PrecioFactory precio3 = new PrecioFactory("UK");
        System.out.println(precio3.getPrecio());
    }
}
