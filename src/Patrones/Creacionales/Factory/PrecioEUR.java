package Patrones.Creacionales.Factory;

/**
 * Estas clases solo tienen el metodo con el valor de cada moneda.
 */
public class PrecioEUR implements Precio{
    @Override
    public double getPrecio() {
        return 1.3;
    }

}
