package Patrones.Comportamiento.Mediator;

/**
 * En esta clase que hereda de la interfaz Colega tenemos el metodo recibe, que solo muestra que ha recibido un mensaje
 * por pantalla y el metodo envia que muestra un mensaje por pantalla y llama al metodo reenvia del mediador.
 */
public class ColegaConcreto1 extends Colega{

    @Override
    void recibe() {
        System.out.println("He recibido un mensaje soy Colega Concreto 1.");
    }

    @Override
    void envia() {
        System.out.println("Soy Colega concreto 1, envio un mensaje.");
        mediator.reenvia(this);
    }
}
