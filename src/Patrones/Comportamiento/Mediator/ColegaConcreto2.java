package Patrones.Comportamiento.Mediator;
// Esta clase es igual a ColegaConcreto1
public class ColegaConcreto2 extends Colega{
    @Override
    void recibe() {
        System.out.println("He recibido un mensaje soy Colega Concreto 2.");
    }

    @Override
    void envia() {
        System.out.println("Soy Colega concreto 2, envio un mensaje.");
        mediator.reenvia(this);
    }
}
