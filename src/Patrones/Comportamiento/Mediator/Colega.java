package Patrones.Comportamiento.Mediator;

/**
 * La interfaz Colega que define los metodos de los objetos que la implementaran y que define el setMediator para
 * asignar el mediador creado en esta interfaz a todos los colegas.
 */
abstract public class Colega {

    public Mediator mediator;

    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    abstract void recibe();

    abstract void envia();
}
