package Patrones.Comportamiento.Mediator;

/**
 * En esta interfaz abstracta definimos los dos metodos que va a tener el mediador.
 */
abstract public class Mediator {

    abstract void registra(Colega colega);

    abstract void reenvia(Colega colega);
}
