package Patrones.Comportamiento.Mediator;

/**
 * Este patron se usa como intermediaro entre dos o mas objetos para controlar y coordinar sus acciones, de tal manera
 * que no necesiten conocer los detalles de la implementacion de los objetos con los que interactuan.
 * En este ejemplo tenemos un mediador que interactua con 3 objetos distintos que se "hablan" entre si.
 */
public class MainMediator {

    public void menuMediator() {
    // Creamos el mediador.
        Mediator mediador = new MediadorConcreto();
    // Y los 3 objetos que van a interactuar usando el mediador.
        Colega colega1 = new ColegaConcreto1();
        Colega colega2 = new ColegaConcreto2();
        Colega colega3 = new ColegaConcreto3();
    // Se registran los objetos en el ArrayList del mediador.
        mediador.registra(colega1);
        mediador.registra(colega2);
        mediador.registra(colega3);
    // Cada uno envia un mensaje y el metodo envia hace que los otros dos lo reciban.
        colega1.envia();
        colega2.envia();
        colega3.envia();

    }
}
