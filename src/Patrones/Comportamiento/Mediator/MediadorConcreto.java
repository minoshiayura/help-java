package Patrones.Comportamiento.Mediator;

import java.util.ArrayList;

/**
 * En la clase Mediador implementamos la lógica. Tenemos un ArrayList que va a guardar a los "colegas". Un metodo para
 * registrarlos en el ArrayList y otro metodo para reenviar el mensaje entre los distintos "colegas".
 */
public class MediadorConcreto extends Mediator {
    //ArrayList de tipo Colega.
    ArrayList<Colega> colegas = new ArrayList<Colega>();
    // Si el ArrayList colegas no contiene el colega que se pasa por parametro se añade y se le asigna el mediador.
    @Override
    void registra(Colega colega) {
        if (!colegas.contains(colega)) {
            colegas.add(colega);
            colega.setMediator(this);
        }
    }
    // Y el metodo reenvia tiene un for que recorre el ArrayList de colegas y si encuentra uno distinto al que se pasa
    // por parametro llama a su metodo recibe().
    @Override
    void reenvia(Colega colega) {
        for (Colega actual : colegas) {
            if (!actual.equals(colega)) {
                actual.recibe();
            }
        }
    }
}
