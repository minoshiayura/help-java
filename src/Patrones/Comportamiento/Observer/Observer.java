package Patrones.Comportamiento.Observer;

/**
 * El patrón Observer sirve para que un objeto conocido como 'Subject' notifique automaticamente a sus dependientes que
 * son conocidos como 'Observers' de cualquier cambio en su estado. En este ejemplo usamos una emisión y varios
 * receptores (TV, Radio y Satelite) estos se meten en un ArrayList y cada vez que se emite se notifica a todos los
 * receptores de ese ArrayList en automatico.
 */
public class Observer {
    public void observer(){
        // Creamos la emisora.
        Emisora emisora = new Emisora();
        // Y los distintos receptores.
        ReceptorTV tv = new ReceptorTV();
        ReceptorRadio radio = new ReceptorRadio();
        ReceptorSatelite sat = new ReceptorSatelite();
        // Los añadimos al ArrayList de la emisora.
        emisora.meteReceptor(tv);
        emisora.meteReceptor(radio);
        emisora.meteReceptor(sat);
        // Y emitimos. Con una sola llamada avisamos a todos los objetos que estan en la lista.
        emisora.emite();
        // Los receptores responderan que han recibido la emisión en automatico.
    }
}
