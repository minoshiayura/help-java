package Patrones.Comportamiento.Observer;

import java.util.ArrayList;

/**
 * Esta clase hace dos cosas, llenar el ArrayList con los receptores y cada vez que se usa el metodo emite recorrer el
 * ArrayList para llamar al metodo recibe de cada receptor.
 */
public class Emisora {
    private ArrayList<Receptor> receptor = new ArrayList<Receptor>();

    public void meteReceptor(Receptor receptor){
        this.receptor.add(receptor);
    }

    public void emite(){
        for(Receptor receptor : receptor){
            receptor.recibe();
        }
    }


}
