package Patrones.Comportamiento.Observer;
// En este ejemplo las clases receptor solo muestran un mensaje de que han recibido el mensaje. Todas implementan la
// interfaz de Receptor.
public class ReceptorRadio implements Receptor{
    @Override
    public void recibe() {
        System.out.println("Señal recibida en radio.");
    }
}
