package Patrones.Comportamiento.Observer;
// La interfaz solo implementa el metodo recibe que es el que tienen que tener todos los receptores.
public interface Receptor {
    void recibe();
}
