package Patrones.Comportamiento.State;

/**
 * En este patrón creamos los metodos y el estado pasandole el objeto telefono. Así nos aseguramos de que todos las
 * clases que hereden de estado tienen que implementar los metodos y podemos ajustarlos para hacer el flujo de
 * trabajo.
 */
abstract public class Estado {
    public Telefono telefono;

    public Estado(Telefono telefono) {
        this.telefono = telefono;
    }

    abstract public String desbloquear();

    abstract public String abrirCamara();

    abstract public String hacerFoto();

}
