package Patrones.Comportamiento.State;

/**
 * El patron State sirve para hacer un workflow. Es decir para que la aplicación funcione de forma ordenada, usando
 * este ejemplo. Que no puedas sacar una foto con el movil bloqueado, antes tienes que desbloquearlo y abrir la cámara.
 */
public class State {
    // El main solo tiene la creación del telefono y las llamadas a los metodos.
    public void state(){

        Telefono tel1 = new Telefono();

        System.out.println(tel1.estado.desbloquear());
        System.out.println(tel1.estado.abrirCamara());
        System.out.println(tel1.estado.hacerFoto());

    }
}
