package Patrones.Comportamiento.State;

/**
 * Aqui es donde se vé el uso del patrón Status. Tenemos el estado bloqueado que hereda de la interfaz Estado. Tiene
 * su constructor que recibe un parametro telefono y que usa el constructor de su padre con el super. Y luego los
 * metodos de Estado para hacer los cambios entre estado. El estado desbloquear que sería el primer paso llama a
 * cambiaEstado pasandole el telefono y creando un nuevo EstadoDesbloqueado. El resto de metodos solo devuelven un
 * string ya que no deben hacer nada si el movil aún no ha llegado al estado necesario.
 */
public class EstadoBloqueado extends Estado{

    public EstadoBloqueado(Telefono telefono) {
        super(telefono);
    }
    @Override
    public String desbloquear() {
        telefono.cambiaEstado(new EstadoDesbloqueado(telefono));
        return "desbloquear(): Movil desbloqueado.";
    }

    @Override
    public String abrirCamara() {
        return "abrirCamara(): La camara está bloqueada, desbloquea el movil antes.";
    }

    @Override
    public String hacerFoto() {
        return "hacerFoto(): La camara está bloqueada, desbloquea el movil antes.";
    }
}
