package Patrones.Comportamiento.State;

/**
 * La clase telefono que lo construye y tiene el metodo de cambio de estado para saber el estado actual, cambiarlo y
 * ver el nuevo estado del telefono. El constructor cuando crea el objeto telefono lo inicia en el estado bloqueado.
 * Así todos los objetos que se crean empiezan en el mismo estado.
 */
public class Telefono {
    public Estado estado;

    public Telefono(){
        estado = new EstadoBloqueado(this);
    }
    public void cambiaEstado(Estado estado){
        System.out.println("Estado inicial: "+this.estado.getClass().getName());
        this.estado = estado;
        System.out.println("Estado final: "+this.estado.getClass().getName());
    }
    public Estado getEstado(){
        return estado;
    }
}
