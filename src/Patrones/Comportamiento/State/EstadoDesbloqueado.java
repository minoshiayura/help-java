package Patrones.Comportamiento.State;

/**
 * Ahora que el movil está desbloqueado se tienen que abrir la camara. Esta clase es igual a EstadoBloqueado, pero aquí
 * el metodo que se usa para cambiar de estado es abrirCamara() que como en la anterior clase pasa el telefono y crea
 * un nuevo estado EstadoCamaraAbierta. Y los otros dos metodos no hacen nada.
 */
public class EstadoDesbloqueado extends Estado{

    public EstadoDesbloqueado(Telefono telefono) {
        super(telefono);
    }
    @Override
    public String desbloquear() {
        return "desbloquear(): El movil ya estaba desbloqueado.";
    }

    @Override
    public String abrirCamara() {
        telefono.cambiaEstado(new EstadoCamaraAbierta(telefono));
        return "abrirCamara(): Camara abierta, puedes hacer la foto.";
    }

    @Override
    public String hacerFoto() {
        return "hacerFoto(): No se puede hacer foto sin abrir la camara.";
    }
}
