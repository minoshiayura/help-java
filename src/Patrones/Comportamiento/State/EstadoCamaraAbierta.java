package Patrones.Comportamiento.State;

/**
 * El tercer estado sería abrir la camara para hacer la foto. Se repite la misma metodología que en los anteriores.
 */
public class EstadoCamaraAbierta extends Estado{
    public EstadoCamaraAbierta(Telefono telefono) {
        super(telefono);
    }
    @Override
    public String desbloquear() {
        return "desbloquear(): El movil ya estaba desbloqueado.";
    }

    @Override
    public String abrirCamara() {
        return "abrirCamara(): La camara ya estaba abierta.";
    }

    @Override
    public String hacerFoto() {
        telefono.cambiaEstado(new EstadoHacerFoto(telefono));
        return "hacerFoto(): Se va a hacer la foto.";
    }
}

