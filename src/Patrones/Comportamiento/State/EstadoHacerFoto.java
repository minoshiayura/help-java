package Patrones.Comportamiento.State;

/**
 * Y en el último estado se usa reutiliza el metodo hacerFoto() pero cambiando el mensaje. Esto se podía haber resuelto
 * con un cuarto metodo también. Y se cambia el estado a EstadoBloqueado.
 */
public class EstadoHacerFoto extends Estado{
    public EstadoHacerFoto(Telefono telefono) {
        super(telefono);
    }
    @Override
    public String desbloquear() {
        return "desbloquear(): El movil ya está desbloqueado.";
    }

    @Override
    public String abrirCamara() {
        return "abrirCamara(): Camara ya esta abierta.";
    }

    @Override
    public String hacerFoto() {
        telefono.cambiaEstado(new EstadoBloqueado(telefono));
        return "hacerFoto(): La foto ya está hecha.";
    }
}