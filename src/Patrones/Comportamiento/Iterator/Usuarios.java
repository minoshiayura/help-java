package Patrones.Comportamiento.Iterator;

import java.util.ArrayList;

/**
 * En esta clase se implementa toda la logica del iterator. Tenemos el ArrayList de usuarios donde se guarda cada
 * usuario que es creado con el metodo crear(). El metodo siguiente que devuelve los usuarios usando el valor de
 * posicion, que se inicializa en 0 y se suma 1 a si mismo en cada iteración. La función hayMas que comprueba si la
 * posicion es menor al tamaño del ArrayList usuarios y reincia que devuleve posicion a su valor 0.
 */
public class Usuarios implements UsuarioIterator{

    private ArrayList<Usuario> usuarios = new ArrayList();
    private int posicion = 0;
    public void crear(Usuario usuario){
        usuarios.add(usuario);
    }
    @Override
    public Usuario siguiente(){
        Usuario usuario = usuarios.get(posicion);
        posicion = posicion +1;
        return usuario;
    }
    @Override
    public boolean hayMas(){
        return posicion < usuarios.size();
    }
    @Override
    public void reinicia(){
        posicion = 0;
    }
}
