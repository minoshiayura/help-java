package Patrones.Comportamiento.Iterator;

/**
 * El patron de diseño Iterator es un patron de comportamiento que permite recorrer una colección de elementos de forma
 * ordenada sin conocer su implementación interna, ya sea una lista, una matriz, ... Se crea una interfaz Iterator que
 * define los metodos necesarios para recorrer la coleccion y una clase que implementa la interfaz y contiene la lógica
 * para recorrer la coleccion, en este ejemplo lo haria la clase Usuarios.
 */
public class Iterator {

    public void iterator() {
        //Creamos los usuarios.
        Usuarios usuarios = new Usuarios();
        usuarios.crear(new Usuario("uno", 5));
        usuarios.crear(new Usuario("dos", 8));
        usuarios.crear(new Usuario("tres", 12));
        usuarios.crear(new Usuario("cuatro", 15));
        // Recorremos la lista usando el metodo hayMas()
        while (usuarios.hayMas()) {
            // El metodo siguiente para mostrar los usuarios creados.
            Usuario usuario = usuarios.siguiente();
            System.out.println("Usuario es " + usuario.getNombre() + " Y con la edad de " + usuario.getEdad());
        }
        // Añadimos uno nuevo y lo mostramos. Esta vez sin el while, ya que el índice está al final de la lista.
        usuarios.crear(new Usuario("cinco", 23));
        Usuario usuario = usuarios.siguiente();
        System.out.println("Usuario es " + usuario.getNombre() + " con la edad de "+usuario.getEdad());
        // Y para volver al principio es necesario usar reinicia(). Así devolvemos el indice al principio
        usuarios.reinicia();
        // y podemos volver a recorrer toda la lista y mostrar todos los usuarios.
        while (usuarios.hayMas()) {
            usuario = usuarios.siguiente();
            System.out.println("Usuario es " + usuario.getNombre() + " Y con la edad de " + usuario.getEdad());
        }
    }
}
