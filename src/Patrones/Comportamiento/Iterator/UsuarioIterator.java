package Patrones.Comportamiento.Iterator;

/**
 * La interfaz iterator que define los metodos.
 */
public interface UsuarioIterator {
    boolean hayMas();
    void reinicia();
    Usuario siguiente();

}
