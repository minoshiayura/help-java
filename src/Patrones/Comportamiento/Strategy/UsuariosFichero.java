package Patrones.Comportamiento.Strategy;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * En esta clase guardamos los usuarios en un fichero.
 */
public class UsuariosFichero implements Usuarios{
    // String para indicar la ruta y nombre del fichero.
    private String ficheroUsuarios = "src/Patrones/Comportamiento/Strategy/usuarios.txt";
    // PrintStream para crear el fichero.
    private PrintStream fichero;
    public UsuariosFichero(){
        try{ //Aqui en el constructor de la clase se crea el fichero, siempre dentro de un try/catch
            fichero = new PrintStream(ficheroUsuarios);
        }catch (Exception e){
            System.out.println("No puedo abrirlo: "+e.getMessage());
        }
    }
    // Metodo crear para añadir el usuario que se pasa como argumento al fichero, en este caso se usa fichero.println
    // con el argumento para que lo añada al fichero.
    @Override
    public void crear(String nombre){
        fichero.println(nombre);
    }
    // Metodo para recorrer el fichero.
    @Override
    public ArrayList<String> listar(){
        // Se crea un ArrayList de tipo String, ya que solo vamos a guardar los nombres de los usuarios.
        ArrayList<String> usuarios = new ArrayList<String>();
        try{
            // Se usa Scanner para leer cada linea del fichero.
            Scanner scanner = new Scanner(new File(ficheroUsuarios));
            // Con el while recorremos el fichero. hasNext() viene a ser un "mientras haya algo que leer en scanner
            // sigue.
            while (scanner.hasNext()){
                // Y añade el usuario del fichero al ArrayList usuarios.
                usuarios.add(scanner.next());
            }
            // Cerramos el scanner.
            scanner.close();
        }catch (Exception e){
            System.out.println("Error leyendo: "+e.getMessage());
        }
        // Devolvemos la lista.
        return usuarios;
    }
}
