package Patrones.Comportamiento.Strategy;
import java.util.ArrayList;

/** Patron Strategy sirve para definir una serie de clases para que sean intercambiables entre si y que el consumidor
 *  no sepa que clase está usando realmente. En este caso se usa con una interfaz que es implementada en los
 *  construtotes. Así aunque cada construtor tenga su forma de funcionar, a la hora de enviar los datos no haya que
 *  hacer trabajo extra adaptando los datos a cada constructor, como se puede ver se envia mismo para UsuariosMemoria
 *  que para UsuariosFichero, ambos tienen los metodos de Usuarios y ya dentro de cada clase se guardan los usuarios
 *  de una forma u otra.
 */
public class Strategy {

    public void strategy(){

        int tipo = 0;
        //Se podría usar un int tipo para dependiendo de que tipo sea se use en un if el constructor necesario.
        if (tipo == 0){
            //UsuariosMemoria usuarios = new UsuariosMemoria();
        }else{
            //UsuariosFichero usuariosfichero = new UsuariosFichero();
        }

        // Creamos los usuarios y los listamos con un for each.
        UsuariosMemoria usuarios = new UsuariosMemoria();
        usuarios.crear("Pepe");
        usuarios.crear("Paco");
        usuarios.crear("Pedro");
        // Esta es otra forma de enviar un usuario nuevo al constructor.
        crear(usuarios, "Roberto");

        for (String usuario : usuarios.listar()){
            System.out.println(usuario);
        }
        // Y aqui se hace lo mismo pero para UsuariosFicheros, que el constructor funciona de otra forma al de Memoria
        // al guardar los datos en un fichero txt.
        UsuariosFichero usuariosfichero = new UsuariosFichero();
        usuariosfichero.crear("Pepe");
        usuariosfichero.crear("Paco");
        usuariosfichero.crear("Pedro");

        crear(usuariosfichero, "Luis");

        for (String usuariofich : usuariosfichero.listar()){
            System.out.println(usuariofich);
        }
    }

    public void crear(Usuarios usuarios, String nombre){
        usuarios.crear(nombre);
    }

    public ArrayList<String> listar (Usuarios usuarios){
        return usuarios.listar();
    }

}
