package Patrones.Comportamiento.Strategy;

import java.util.ArrayList;
// Este metodo usa un ArrayList para guardar y mostrar los usuarios.
public class UsuariosMemoria implements Usuarios {
    private ArrayList<String> usuarios = new ArrayList<String>();
    @Override
    public void crear(String nombre){
        usuarios.add(nombre);
    }
    @Override
    public ArrayList<String> listar(){
        return usuarios;
    }
}
