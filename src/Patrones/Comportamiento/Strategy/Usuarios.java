package Patrones.Comportamiento.Strategy;

import java.util.ArrayList;
// Interfaz que va a implementar los metodos que tienen que tener los constructores.
public interface Usuarios {
    // En este caso solo crear usuario y listar usuarios.
    void crear(String nombre);

    ArrayList<String> listar();

}
