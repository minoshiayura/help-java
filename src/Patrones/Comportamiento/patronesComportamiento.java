package Patrones.Comportamiento;

import Patrones.Comportamiento.Iterator.Iterator;
import Patrones.Comportamiento.Mediator.MainMediator;
import Patrones.Comportamiento.Observer.Observer;
import Patrones.Comportamiento.State.State;
import Patrones.Comportamiento.Strategy.Strategy;
import Patrones.Patrones;

import java.util.Scanner;

public class patronesComportamiento {
    public void menuComportamiento(){
        System.out.println("Este es el menu de los patrones de comportamiento.");
        System.out.println("Elige un patron que ver: ");
        System.out.println("Iterator, Mediator, Observer, State, Strategy");
        Scanner scanner = new Scanner(System.in);
        String choice = scanner.next();
        choice = choice.toLowerCase();
        switch (choice) {

            case "iterator":
                System.out.println("Has seleccionado patron Iterator");
                Iterator ite = new Iterator();
                ite.iterator();
            break;

            case "mediator":
                System.out.println("Has seleccionado patron Mediator");
                MainMediator medi = new MainMediator();
                medi.menuMediator();
            break;
            case "observer":
                System.out.println("Has seleccionado patron Observer");
                Observer obse = new Observer();
                obse.observer();
            break;
            case "state":
                System.out.println("Has seleccionado patron State");
                State stat = new State();
                stat.state();
            break;
            case "strategy":
                System.out.println("Has seleccionado patron Strategy");
                Strategy strat = new Strategy();
                strat.strategy();
            break;
            default:
                Patrones patr = new Patrones();
                patr.menuPatrones();
            break;
        }
    }
}
