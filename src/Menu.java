import Excepciones.Excepciones;
import Patrones.Patrones;

import java.io.IOException;
import java.util.Scanner;

public class Menu {

	public void menu() throws IOException {
		
		System.out.println("Ayuda en:");
		System.out.println(" 0. Basico");
		System.out.println(" 1. Condicionales: if, for, switch, do y do while");
		System.out.println(" 2. Array");
		System.out.println(" 3. Programacion funcional y recursiva");
		System.out.println(" 4. Excepciones.Excepciones");
		System.out.println(" 5. Patrones de diseño");
		System.out.println(" 6. ");
		System.out.println(" 7. ");
		System.out.println(" 8. ");
		System.out.println(" 9. Cerrar");
		System.out.println("Escoge uno");
		
		//Con Scanner pedimos que se introduzca el numero por teclado.
		Scanner s = new Scanner(System.in);
		String choice = s.next(); //Igualamos s a choice, ya que el switch necesita un int y s es scanner.
								  //Por eso hacemos el parse de s.nextInt() para guardar como int s en choice
		System.out.println("");
		
		switch (choice) {
			case "0":
				Basico t = new Basico();
				t.basico();
			break;
			case "1":
				System.out.println("Selecciona que condicional quieres ver: ");
				System.out.println("if - for - switch - do - while");
				choice = s.next();
				if(choice.equalsIgnoreCase("if")){
					Condicional i = new Condicional();
					i.ifs();
				}else if(choice.equalsIgnoreCase("switch")){
					Condicional sw = new Condicional();
					sw.swit();
				}else if(choice.equalsIgnoreCase("for")){
					Condicional f = new Condicional();
					f.fo();
				}else if (choice.equalsIgnoreCase("do")) {
					Condicional wh = new Condicional();
					wh.whil();
				}else if (choice.equalsIgnoreCase("while")){
					Condicional dwh = new Condicional();
					dwh.dowhil();
				}else{
					this.menu();
				}
			break;
			
			case "2":
				Array ar = new Array();
				ar.arrays();
			break;
			
			case "3":
				System.out.println("Que quieres ver programacion funcional (f) o recursiva (r)");
				char fr = (char) System.in.read();

				if (fr == 'f'){
					Funcionales fun = new Funcionales();
					fun.pruebasFuncional();
				}else if(fr == 'r'){
					Recursividad rec = new Recursividad();
					rec.pruebaRecursiva();
				}else{
					this.menu();
				}
			break;
			
			case "4":
				Excepciones exc = new Excepciones();
				try {
					exc.controlExcepciones();
				}catch(Exception e){
					e.getMessage();
				}
				this.menu();
			break;
			
			case "5":
				Patrones patr = new Patrones();
				patr.menuPatrones();
				this.menu();
			break;
			
			case "6":

			break;

			case "7":

			break;

			case "8":

			break;

			case "9":
				System.out.println("Cerrado.");
			break;

			default:
				//Esta opción se seleccionará en el caso de que se introduzca por teclado algo que no sea
				//Uno de los numeros establecidos en los distintos case
				System.out.println("Opción no encontrada.");
				//Y devolvera el programa al principio usando this
				this.menu();
		}
		s.close();
	}	

}
