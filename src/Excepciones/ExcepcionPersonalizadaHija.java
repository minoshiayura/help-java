package Excepciones;

public class ExcepcionPersonalizadaHija extends ExcepcionPersonalizada{
    // Tambien se pueden crear clases de excepciones que hereden de otra personalizada. Su uso es igual que la anterior.
    // Se establece la herencia y un constructor. En este caso he personalizado el mensaje, también se puede poner un
    // StackTraceElement o lo que se necesite.
    public ExcepcionPersonalizadaHija(String message){
        super("Este mensaje es personalizable "+message);
    }
}
