package Excepciones;

import java.io.IOException;

public class Excepciones {

    public void controlExcepciones() throws ExcepcionPersonalizada, IOException {
        /**
         * Las excepciones sirven para controlar los fallos que puedan surgir durante la ejecucion del programa. Se
         * pueden hacer de varias formas. Se crea con un bloque try{-codigo-}catch(Excepcion){-codigo-}. Siempre se
         * tiene que poner el catch.
         */
        System.out.println("Aqui hay varios ejemplos de control de excepciones.");
        System.out.println("Primero el uso de Exception.");
        excepcion();
        System.out.println("Ahora con la excepcion aritmetica.");
        excepcionAritmetica();
        System.out.println("Usando StackTraceElement para ver la traza.");
        excepcionStack();
        System.out.println("Un ejemplo con finally.");
        excepcionFinally();
        System.out.println("En este ejemplo se usa una excepcion personalizada.");
        excepcionPersonalizada();
        System.out.println("Y en este una excepcion personalizada que hereda de la anterior.");
        excepcionPersonalizadaHija();


    }
    // En este primer ejemplo tenemos el catch con Exception, que es la clase padre de las excepciones, controla todas.
    public void excepcion(){
        int valor = 5;
        try{
            valor = valor /0;
        }catch(Exception e){
            System.out.println("Error de Exception: "+e.getMessage());
        }
    }
    // Aquí usamos ArithmeticException, que es la clase dedicada a este tipo de excepciones.
    public void excepcionAritmetica(){
        int valor = 5;
        try{
            valor = valor /0;
        }catch(ArithmeticException e){
            System.out.println("Error de Exception: "+e.getMessage());
        }
    }
    // El stack muestra la traza donde ha fallado, se puede personalizar para que muestre tanta información como
    // necesitemos.
    public void excepcionStack(){
        int valor = 5;
        try{
            valor = valor /0;
        }catch(ArithmeticException e){
            System.out.println("Error de Exception: "+e.getMessage());

            for (StackTraceElement val : e.getStackTrace()) {
                System.out.println("Linea: " + val.getLineNumber() + " " + val.getMethodName());
            }
        }
    }
    // Se puede crear una clase exception personalizada, por si por ejemplo, tenemos una clase Usuario y queremos que
    // las excepciones que se lancen sean personalizadas al metodo que falla. Como crear o listar.
    public void excepcionPersonalizada() throws ExcepcionPersonalizada{
        int valor = 5;
        try{
            valor = valor /0;
        }catch(ArithmeticException e){
            throw new ExcepcionPersonalizada(e.getMessage());
        }
    }
    // Y podemos hacer una clase excepcion que herede de otra personalizada, siguiendo el ejemplo del anterior, la
    // primera clase personalizada sería para Usuario y luego se crearian otras para manejar las excepciones de los
    // distintos metodos que tendría esa clase, como crear, listar, borrar ...
    public void excepcionPersonalizadaHija() throws ExcepcionPersonalizadaHija {
        int valor = 5;
        try{
            valor = valor /0;
        }catch(ArithmeticException e){
            throw new ExcepcionPersonalizadaHija(e.getMessage());
        }
    }
    // El finally es opcional. Si se pone lo que haya dentro se va a ejecutar siempre, salte alguna excepcion o no. Se
    // puede usar si se esta trabajando con ficheros para cerrarlos o con conexiones a base de datos. Para ocurra lo
    // que ocurra siempre se cierre.
    public void excepcionFinally(){
        int valor = 5;
        try{
            valor = valor /0;
        }catch(Exception e){
            System.out.println("Error de Exception: "+e.getMessage());
        }finally {
            System.out.println("Esto se ejecuta siempre.");
        }
    }
}
