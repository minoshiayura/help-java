package Excepciones;

public class ExcepcionPersonalizada extends Exception{
    // La clase de excepcion personalizada hereda de Exception que es la clase padre de todas las excepciones.
    // Solo hay que crear un constructor de la clase con un String message como argumento y un super(message) para el
    // mensaje a mostrar.
    public ExcepcionPersonalizada (String message){
        super(message);
    }
}
