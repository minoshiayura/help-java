import java.io.IOException;
import java.util.Scanner;

public class Array {
	
	public void arrays() throws IOException {
		
		System.out.println("Menu de Arrays");
		System.out.println("Elige la opción");
		System.out.println(" 0.¿Que es un Array?");
		System.out.println(" 1. Ejemplo ordenar números");
		System.out.println(" 2. Array multidimensional");
		System.out.println(" 3. Arrays irregulares");
		System.out.println(" 4. Iniciar un Array");
		System.out.println(" 5. Uso de length");
		System.out.println(" 9. Salir");
		
		Scanner ch = new Scanner(System.in);
		int s = ch.nextInt();
		
		Menu m = new Menu();
		
		switch (s) {
		
		case 0:
			
			this.expArray();
			
		break;
		
		case 1:
			
			this.ordenaArray();
			
		break;
		
		case 2:
			
			this.dosDimen();
			
		break;
		
		case 3:
			
			this.irregular();
			
		break;
		
		case 4:
			
			this.iniciar();
			
		break;
		
		case 5:
			
			this.len();
			
		break;
		
		case 9:
			
			System.out.println("Saliendo de menu Arrays.");
			m.menu();
			
		break;
		
		default:
			
			System.out.println("Opción no encontrada.");
			this.arrays();
			
		}
		
		ch.close();
	}
	
	void expArray () throws IOException{
		
		System.out.println("Los arrays son colecciones de variables del mismo tipo. Pueden ser"
				+ "int, char, String o de objetos como Vehiculo, Persona...");
		System.out.println("Se declaran como 'tipo [] nombre variable = new tipo [tamaño]; ' ");
		System.out.println("Ejemplo 'int[] numeros = new int [10]; ' esto seria un Array de int con 10 huecos");
		System.out.println("Recuerda se cuenta desde el 0. En el array de ejemplo son 10 elementos, van del 0 al 9");
		System.out.println("");
		System.out.println("Los arrays pueden tener varias dimensiones, sería como un Array de arrays"
				+ ". pueden ser de 2 o mas dimesiones.");
		System.out.println("Ejemplo declarar array 2 dimensiones: int [][] nombre = new int [3][4]; ");
		System.out.println("El ejemplo es el array multidimensional mas simple, el de 2 dimensiones. "
				+ "El cual sería como una tabla de 3 filas y 4 columnas. Para añadir mas dimensiones solo hay que "
				+ "poner mas [] en la declaración.");
		System.out.println("No es necesario declarar el tamaño de todas las dimensiones al principio"
				+ ". Solo es obligatorio declarar la primera dimension, la que está mas a la izquierda. "
				+ "El resto se pueden declarar mas adelante y de forma individual, ejemplo en Arrays irregulares.");
		System.out.println("");
		this.arrays();
	}
	
	void ordenaArray() throws IOException {
		
		System.out.println("Vamos a crear un array de números aleatorios y ordenarlos.");
		System.out.println("Los números los generaremos con Math.Random");
		
		int r, a, b, t, size=10; //Este size nos ayudara para ordenar, se refiere al tamaño del array pero esto se
		// indica en la declación.
		int[] arnum = new int [10]; //Declaramos el Array y su tamaño que será 10.
		
		for (int i=0; i<10; i++) {//Recuerda empezar el for por 0			
			r = (int)(Math.random()*100); //Math.random es para generar números aleatorios y el *100 indica el
			// numero mas alto.
			arnum[i]=r;	//Con este for recorremos array y en la posicion de i metemos r. ç
		}				//Así hay un numero aleatorio en cada hueco de array.
		
		System.out.println("El array original es: ");
		for (int y=0; y<10; y++) { //For para recorrer el array
			System.out.print(" "+arnum[y]); 
			
		}
		
		for (a=1; a < size; a++) { //Este for recorre los 10 elementos de array
			for (b=size-1; b>=a; b--) {//Este se compara con a, mientras sea mayor continuará funcionando
				if (arnum[b-1]>arnum[b]) {//Con está función ordenamos el array
					t= arnum[b-1];
					arnum[b-1]=arnum[b];
					arnum[b]=t;
				}
				
			}
			
		}
		System.out.println("");
		System.out.println("Array ordenado: ");
		for (int u=0; u<size; u++) {//Con este for mostramos el array ordenado.
			System.out.print(" "+arnum[u]);
		}
		System.out.println("");
		this.arrays();
	}
	
	void dosDimen() throws IOException {
		
		System.out.println("Crearemos un array de 2 dimensiones como el del ejemplo.");
		
		int t, i;
		int [][] tabla = new int [3][4]; //Declaramos el array de 2 dimensiones
		
		for(t=0; t<3; t++) {//Con este for recorremos la primera dimension
			for(i=0; i<4; i++) { //Con este la segunda
				tabla[t][i]= (t*4)+i+1; //Añadimos a cada hueco un numero del 1 al 12
				System.out.print(tabla[t][i]+" "); //los mostramos por pantalla
			}
			System.out.println();
		}
		System.out.println("");
		this.arrays();
	}
	
	void irregular() throws IOException {
		
		System.out.println("Los arrays irregulares son multidimensionales, pero sus dimensione "
				+ "no tiene todas el mismo tamaño.");
		System.out.println("Ejemplo: int tabla[][] = new int[3][]; ");
		System.out.println("Se puede establecer el tamaño de las dimesiones de forma separada.");
		System.out.println("¿Quieres ver un ejemplo? S o N.");
		char e = (char)System.in.read();
		
		if(e=='s'|e=='S') {
			
			System.out.println("Ejemplo array irregular");
			/*
			 * En este ejemplo vamos a hacer un programa que muestre los pasajeros de una aerolinea
			 * que hace viajes a diario, pero de lunes a viernes hace 10 vuelos y en fin de semana
			 * solo 2. Para eso usamos un array irregular que en su primera dimension tenga los 7 días
			 * de la semana, pero en su segunda contenga el numero de viajes que hace cada día. Como
			 * no son los mismo los declararemos de forma separada.
			 */
			int[][] pasajeros = new int[7][]; //Declaramos el array con los días de la semana en la primera dimension,
			// dejando vacia la segunda.
			pasajeros[0]=new int [10]; // le decimos que en la segunda dimensión son 10 elementos.
			pasajeros[1]=new int [10]; //Y seguimos con el resto de días.
			pasajeros[2]=new int [10];
			pasajeros[3]=new int [10];
			pasajeros[4]=new int [10];
			pasajeros[5]=new int [2]; //Hasta llegar al fin de semana que son solo 2 elementos.
			pasajeros[6]=new int [2];
			
			int i,j;
			
			for (i=0; i<5; i++) //Este for se situará en cada uno de los primeros 5 días. 
				for (j=0; j<10; j++) //Este se situará en cada uno de los 10 vuelos.
					pasajeros[i][j] = i + j + 10; //Y esto rellenará la segunda dimensión con los pasajeros
			for(i=5; i<7; i++) //Este es para el fin de semana, por eso empieza en el elemento 5
				for(j=0; j<2; j++)
					pasajeros[i][j]= i + j + 10;
			System.out.println("Pasajeros por viaje durante dias laborables de la semana: ");
			for(i=0; i<5; i++) { //Con estos for mostramos cada vuelo de los días laborables.
				for (j=0; j<10; j++){
					System.out.print(pasajeros[i][j]+" ");
					System.out.println();
				}
			}
			System.out.println();
			System.out.println("Pasajeros por viaje durante el fin de semana");
			for (i=5; i<7; i++) { //Y con estos los del fin de semana.
				for (j=0; j<2; j++)
					System.out.print(pasajeros[i][j]+" ");
				System.out.println();
			}
		}
		System.out.println("");
		this.arrays();
		
	}
	
	void iniciar() throws IOException {
		
		int[][] cuadrado = {
				{1,1},
				{2,4},
				{3,9},
				{4,16},
				{5,25},
				{6,36},
				{7,49},
				{8,64},
				{9,81},
				{10,100}
		};
		int i,j;
		
		for(i=0;i<10;i++) {
			for(j=0;j<2;j++)
				System.out.print(cuadrado[i][j]+" ");
			System.out.println();
			
		}
		System.out.println("");
		this.arrays();
	}
	
	void len() throws IOException {
		
		System.out.println("El uso de .length en un Array es para averiguar su tamaño,"
				+ "o lo que es lo mismo, el numero de elementos que puede almacenar.");
		
		int [] list = new int [10]; //Estas son 3 formas de iniciar los arrays
		int [] nums = {1,2,3};
		int [][] tabla = {
				{1,2,3},
				{4,5},
				{6,7,8}
		};
		//Y en todas se usa .length de la misma forma. 
		System.out.println("Longitud de list es: "+ list.length);
		System.out.println("Longitud de nums es: "+ nums.length);
		System.out.println("Longitud de tabla es: "+ tabla.length);//Nos da cuantos arrays contiene la tabla
		System.out.println("Longitud de tabla[0] es: "+ tabla[0].length);//Y aqui el de cada array
		System.out.println("Longitud de tabla[1] es: "+ tabla[1].length);
		System.out.println("Longitud de tabla[2] es: "+ tabla[2].length);
		System.out.println();
		
		for(int i=0; i<list.length; i++)//Aquí usamos el .length para dar el tamaño del array
			list[i]=i*i;
		System.out.print("El contenido de lista es: ");
		for(int i=0; i<list.length; i++)
			System.out.print(list[i]+" ");
		System.out.println();
		//En esta parte usamos length para comprobar el tamaño de un array y ver si podemos copiar
		//otro en el.
		int[] num1 = new int [10];
		int[] num2 = new int [10];
		
		System.out.println("Contenido de num1: ");
		for(int i=0; i<num1.length; i++) {
			num1[i]=i+2;
			System.out.print(num1[i]+" ");
		}
		System.out.println();
		if(num2.length>=num1.length)
			for(int i=0;i<num1.length;i++)
				num2[i]=num1[i];
		System.out.println("Contenido de num2: ");
		for(int i=0; i<num2.length; i++)
			System.out.print(num2[i]+" ");
		
		System.out.println("");
		this.arrays();
	}

}
