import java.io.IOException;
import java.util.Arrays;
import java.util.function.Function;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Funcionales {

    //Lambda y metodo. Lo mismo de dos formas.
    // Lambda es una funcion pura.
    private Function<String, String> toMayus = (x) -> x.toUpperCase();
    // este metodo es un ejemplo para hacer lo mismo que la lambda pero en mas pasos.
    public static String toMayuscula(String nombre){
        return nombre.toUpperCase();
    }
    // Otro ejmplo de una lambda.
    private Function<Integer, Integer> sumador = (x) -> x.sum(x,x);

    public void pruebasFuncional() throws IOException {
        System.out.println(toMayuscula("jose")); //Llamamos a las lambdas y metodos. El resultado es el mismo.
        System.out.println(toMayus.apply("jose"));

        System.out.println(sumador.apply(5));
        // Creamos un arraylist como ejemplo.
        ArrayList<String> nombres = new ArrayList<String>();
        nombres.add("jose");
        nombres.add("paco");
        nombres.add("pepe");
        // En lugar de recorrerla con un for podemos mostrarla de esta forma.
        nombres.stream().forEach(x -> System.out.println(x));
        // Es lo mismo que arriba pero usando la version corta de una lambda.
        nombres.stream().forEach(System.out::println);
        // map mapea lo que este en el stream y lo pasa a x, conviertiendolo en mayusculas en este caso
        Stream<String> valores = nombres.stream().map(x -> x.toUpperCase());
        // Y luego pasamos valores por forEach para mostrar cada item de valores con un println
        valores.forEach(x -> System.out.println(x)); //El problema es que no podemos reutilizar el stream "valores".
        // Si lo hicieramos el progama fallaría ya que los streams se consumen en su uso. Tendriamos que crear otro
        // stream con otro nombre que recorra el mismo, en este caso ArrayList.
        // Podemos añadir mas funciones que obtendran sus elementos de la anterior. En este caso filter obtiene los
        // valores de map.
        Stream<String> valores2 = nombres.stream().map(x -> x.toUpperCase())
                .filter(x -> x.startsWith("P"));
        valores2.forEach(x -> System.out.println(x));
        // Podemos convertir un array de numeros en un stream
        int []numeros = {1,2,3,4,5,6,7,8,9,10};
        var stNumeros = Arrays.stream(numeros);
        // Y luego podemos iterar sobre ellos con un forEach
        stNumeros.forEach(x -> System.out.println(x));
        // Volvemos a definir un array de numeros
        int []nums = {1,2,3,4,5,6,7,8,9,10};
        var stNums = Arrays.stream(nums); //Lo guardamos en un stream, pero ahora lo filtramos con filter para que le
        // pase a reduce los numeros del array que su resto sea igual a 0. Entonces reduce va sumando los valores que le
        // pasa filter entre ellos de modo que y son los valores que pasan el filtro y x la suma de ellos. El 0 indica
        // el valor inicial de x.
        var resuNums = stNums.filter(x -> x % 2 == 0).reduce(0, (x,y) -> {
            System.out.println("X es: "+ x + " Y es: " + y);
            return x + y; // Las lambda permite escribir el codigo que se quiera siempre que sea dentro de los {}
            // y la única norma que hay que respetar es que termine con un return.
        } );
        System.out.println("La suma total de los valores de X es: "+resuNums);

        System.out.println(" ");
        Menu m = new Menu();
        m.menu();
    }
}

