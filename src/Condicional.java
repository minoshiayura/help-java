import java.io.IOException;
import java.util.Scanner;

public class Condicional {

Menu m = new Menu();
	
	
	public void ifs() throws IOException {
		
		System.out.println("El if: \n");
		System.out.println("if es condicional");
		System.out.println("Se usa:");
		System.out.println("if (a == b) { \n codigo} \n else {codigo}");
		System.out.println("Se puede dejar solo o poner un else después.\n"
				+ " Se pueden concatenar varios else if para las distintas opciones. "
				+ "Y también se pueden concatenar varios if. Los else van con el if mas cercano");
		
		System.out.println("¿Quieres ver un ejemplo? S , N o E (Para el else if)");
		char ch = (char) System.in.read(); //Otra forma de pedir que se introduzca algo por teclado.
		System.out.println("");
			if(ch == 's' | ch == 'S') {
				System.out.println("Si elegiste s y lees esto estás dentro del if");
			}else if (ch == 'e' | ch == 'E') {
				System.out.println("Si elegiste e estas en el else if");
			}else {
				System.out.println("Si elegiste n o cualquier otra letra estas en el else");
			}
			
			System.out.println("");
		m.menu();
	}
	
	public void swit() throws IOException {
		
		System.out.println("El Switch \n");
		System.out.println("Es para que el usuario elija entre varias opciones");
		System.out.println("Se usa:");
		System.out.println("switch (a)\n case 1 \n codigo \n break; case 2 ...");
		System.out.println("La a puede ser una letra o un número.\n Si se pide por pantalla"
				+ " con Scanner el número pasar con .nextInt a int. Y si es un char usar "
				+ "char ch = (char) System.in.read(); No se puede pasar de Scanner a char. "
				+ "Y se puede poner un switch dentro de otro.");
		
		System.out.println("");
		m.menu();
		
	}
	
	public void fo() throws IOException {
		
		
		System.out.println("Elija que for quiere ver:");
		System.out.println(" 0. for");
		System.out.println(" 1. for each/for mejorado");
		System.out.println(" 2. for each array multidimensional");
		System.out.println(" 9. salir.");
		
		Scanner s = new Scanner(System.in);
		int choice = s.nextInt();
		
		switch (choice) {
		
		case 0:
			
			System.out.println("El for: \n");
			System.out.println("Es un bucle donde se da una incializacion, una condicion y una iteracion.");
			System.out.println("Se usa:");
			System.out.println("for(i=1; i<100; i++)\n {codigo}");
			System.out.println("El loop se repetira hasta que la variable de inicio sea igual a la condicion.");
			System.out.println("Se pueden añadir varias variables para controlar el loop usando comas\n"
					+ " for(int i=0, j=30; i<23; i++, j--)");
			
			System.out.println("¿Quieres ver un ejemplo? S o N");
			char ch = (char) System.in.read();
			System.out.println("");
				if( ch =='S' | ch == 's') {

					for(int i=0;i<10;i++) {
						System.out.println("El valor de i es ="+i);
					}
					System.out.println("");
					System.out.println("Cada iteración del for suma 1 al valor de i");
				}
				System.out.println("");
				this.fo();
			
		break;
		
		case 1:
			
			System.out.println("Otra opción del for es el for-each o for mejorado.");
			System.out.println("Se declara: ");
			System.out.println("for (tipo nombre : colecccion) { codigo }");
			System.out.println("Lo usamos para recorrer arrays");
			System.out.println("¿Quieres ver un ejemplo de un for each?");
			System.out.println("S o N?");
			
			char cha=(char) System.in.read();
			System.out.println("");
				if(cha == 's' || cha == 'S') {
					//Vamos a crear un array con 10 números y vamos a usar un for each para obtener la suma de todos ellos.
					int[] nums = {1,2,3,4,5,6,7,8,9,10};
					int sum=0;
					System.out.println("En este ejemplo recorremos un array de una sola dimensión");
					for(int x : nums) {
						//Declaramos x y recorremos el array nums
						System.out.println("El valor es :"+x);
						sum += x; //Aqui añadimos el valor de x sumado al valor de sum.
					}
					System.out.println("Suma: "+sum); //Muestra el total de la suma de todos los elementos del array.
					
					System.out.println("Y ahora vamos a usar for each para buscar un elemento concreto dentro de un array");
					
					int val=5;//valor a encontrar
					boolean encontrado = false; //booleano para devolver si encuentra el valor
					
					for(int x : nums) { //Usamos for each para recorrer array
						if (x == val) { //Y con este if comprobamos si x es igual al valor que queremos encontrar
							encontrado = true; // Si lo encuentra cambia encontrado a true
							break;//Y con este break salimos del for sin necesidad de terminar todo el array
						}
					}
					if(encontrado) //Este if solo con la variable encontrado funciona si la variable es igual a true, si no no entra dentro del if
						System.out.println("Valor encontrado"); //Si es true encontrado muestra este mensaje
				}			
			System.out.println("");
			this.fo();
			
		break;
		
		case 2:
			
			System.out.println("Ahora vamos a ver un ejemplo de for each para array multidimensional");
			
			int sum = 0;
			int [][] nums = new int  [3][5];
			
			for(int i = 0; i<3; i++) //Rellenamos el array multidimensional
				for(int j=0; j<5; j++)
					nums[i][j]=(i+1)*(j+1);
			
			for(int[]x : nums) {//Declaramos x para la primera dimensión de esta forma
				for(int y : x) { //Y la declaramos para que recorra x
					System.out.println("El valor es: "+y);
					sum+=y;
				}
			}
			System.out.println("Suma: "+sum);
			
			System.out.println("");
			this.fo();
			
		break;
		
		case 9:
			System.out.println("");
			m.menu();
		break;
		
		default:
			System.out.println("Opción no encontrada.");
			System.out.println("");
			this.fo();
		break;
		
		}
		s.close();
		
	}
	
	public void whil() throws IOException {
		
		System.out.println("El while: \n");
		System.out.println("Es un bucle que se repite mientras su condicon sea cierta"
				+ ". Necesita que la condicion sea una expresion booleana valida");
		System.out.println("Se usa:");
		System.out.println("while (condicion){codigo}");
		
		System.out.println("¿Quiere un ejemplo? S o N");
		char ch = (char) System.in.read();
		System.out.println("");
			if (ch == 'S' | ch == 's') {
				char wh='a';
				while(wh <= 'z') {
					System.out.println(wh);
					wh++;
				}	
			}
			
			System.out.println("");
		m.menu();
	}
	
	public void dowhil() throws IOException {
		
		System.out.println("El do while: \n");
		System.out.println("En este bucle la condicón se comprueba al final, por eso "
				+ "siempre se ejecuta al menos una vez");
		System.out.println("Se usa:");
		System.out.println("do { codigo a ejecutar; } while (condicion);");
		
		System.out.println("¿Quieres un ejemplo? S o N");
		char ch = (char) System.in.read();
		System.out.println("");
			if (ch == 'S' | ch == 's') {
				char dow;
				do {
					System.out.println("Pulsa cualquier tecla para hacer el bucle, Q para salir");
					dow = (char) System.in.read();
				}while(dow != 'q');
			}
			
			System.out.println("");
		m.menu();
	}
	
	
}
